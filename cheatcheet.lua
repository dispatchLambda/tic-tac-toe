-- Mit variablen kannst du Werte speicher
num = 42

-- Du kannst unter anderem Zahlen und Zeichenketten speichen
zahl = 43
greeting = 'hello world' -- Eine Zeichenkette beginnt und endet mit einem '

-- Bedingungen führen Code genau einmal aus wenn eine best. Bedingung wahr ist
isColdOutside = true
if isColdOutside then
    print('It\'s probably summer')
end

-- Wenn ein du unterschiedliche Sachen machen willst wenn etwas wahr oder falsch ist
-- kannst du festlegen was passiert wenn die Bedingung falsch ist
isWarmOutside = false
if isWarmOutside then 
    print('It\'s probably summer')
else 
    print('It\'s probably winter')
end

-- Mit 'not' kannst du den Wahrheitswert einer Bedingung umkehren
if not isWarmOutsidde then 
    print('It\'s probably winter')
end

-- Mit 'and' kannst du zwei Bedingungen zu einer zusammenfügen,
-- die wahr ist wenn die beiden uhrsprünglichen Bedingungen wahr sind
if isWarmOutside and isColdOutside then
    print('This can\'t be true')
end

-- Bei 'or' reicht es wenn eine der beiden Bedingungen wahr ist
age = 190
if age > 130 or age < 0 then
    print('This can\'t be true')
end

-- Mit Schleifen kannst du einen Codeabschnitt beliebig oft ausführen
counter = 0
while counter < 50 do -- Der Teil zweichen while und do ist die Bedingung.
    -- Damit der Code zwischen do und end ausgeführt wird,
    -- muss die Bedingung wahr sein
    counter = counter + 1 
end

-- Mit for-Schleifen kannst du etwas bestimmt oft durchführen und eine
-- variable hochzählen lassen
for i = 1, 100 do 
    print(i)
end
-- Output:
-- 1
-- 2
-- ...
-- 100

-- Mit Funktionen kannst du einem Stück Code einen Namen geben
function spamGreeting() do 
    for i = 1, 100 do 
        print('Hello')
    end
end

spamGreeting() -- Du kannst diesen Code jetzt beliebig oft aufrufen

-- Wenn du mehr als eine Sache in einer Variable speichern willst
-- kannst du Tabellen benutzen. Sie beinhalten beliebig viele Schlüssel
-- mit dazugehörigen Werten.
example_table = {
    name = "James", -- Links vor dem '=' steht immer der Schlüssel und rechts der Wert
    surname = "Bond",
    ltk = true
}
-- Um auf einen Wert zuzugreifen schreibt man Variablenname.Schlüssel
example_table.name -- => "James"

-- Wenn du den Schüssel in eckigen Klammern schreibst kannst 
-- du alles als Schlüssel verwenden
foo = {
    [100] = "bar",
    [false] = 3,
    ["hallo"] = "hello"
}
-- Du kannst auf die Werte dann mit eckigen Klammern zugreifen
foo[false] -- => 3

-- Wenn du keine Schüssel angibst, werden automatisch Zahlen verwendet
bar = {
    false, 3, 4, "random", 3421, -9
}
bar[1] -- => false
bar[4] -- => "random"
