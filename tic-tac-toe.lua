st={
[1]= {"##\\          /##",
      " \\##\\      /##/ ",
      "   \\##\\  /##/   ",
      "     \\####/     ",
      "     /####\\     ",
      "   /##/  \\##\\   ",
      " /##/      \\##\\ ",
      "##/          \\##"},
      
[2]= {"   __________   ",
      "  / ________ \\  ",
      " / /        \\ \\ ",
      "| |          | |",
      "| |          | |",
      "| |          | |",
      " \\ \\________/ / ",
      "  \\__________/  "},
      
[0]= {"                ",
      "                ",
      "                ",
      "                ",
      "                ",
      "                ",
      "                ",
      "                "}}

sp = "#"

-- ##\          /##/  -    ___________
--  \##\      /##/    -   / _________ \
--    \##\  /##/      -  / /         \ \
--      \####/        - | |           | |
--      /####\        - | |           | |
--    /##/  \##\      - | |           | |
--  /##/      \##\    -  \ \_________/ /
-- ##/          \##   -   \___________/
   ----------------     -----------------

tpf = {{0,0,0},{0,0,0},{0,0,0}}

function draw()
 r=1
 for i=1,8 do
  print(st[tpf[r][1]][i].." "..string.rep(sp,3).." "..st[tpf[r][2]][i].." "..string.rep(sp,3).." "..st[tpf[r][3]][i])
 end
 
 print(string.rep("#",(3*16+2*5)))
 print(string.rep("#",(3*16+2*5)))
 
 r=2
 for i=1,8 do
  print(st[tpf[r][1]][i].." "..string.rep(sp,3).." "..st[tpf[r][2]][i].." "..string.rep(sp,3).." "..st[tpf[r][3]][i])
 end
 
 print(string.rep("#",(3*16+2*5)))
 print(string.rep("#",(3*16+2*5)))
 
 r=3
 for i=1,8 do
  print(st[tpf[r][1]][i].." "..string.rep(sp,3).." "..st[tpf[r][2]][i].." "..string.rep(sp,3).." "..st[tpf[r][3]][i])
 end
 
end

function test()
 --test from top left corner
   t=tpf[1][1]
   if tpf[1][2]==t and tpf[1][3]==t then return mapp[t] end --horizontal
   if tpf[2][2]==t and tpf[3][3]==t then return mapp[t] end --diagonal
   if tpf[2][1]==t and tpf[3][1]==t then return mapp[t] end --vertical
 --test from middle
   t=tpf[2][2]
   if tpf[2][1]==t and tpf[2][3]==t then return mapp[t] end --horizontal
   if tpf[3][1]==t and tpf[1][3]==t then return mapp[t] end --diagonal
   if tpf[1][2]==t and tpf[3][2]==t then return mapp[t] end --vertical
 --test from bottom right corner
   t=tpf[3][3]
   if tpf[3][2]==t and tpf[3][1]==t then return mapp[t] end --horizontal
   if tpf[2][3]==t and tpf[1][3]==t then return mapp[t] end --vertical
 --test if nobody won
 --nbody won this round and playfield is full
 n = true;
 for i=1,3 do
  for j=1,3 do
   if tpf[i][j] == 0 then n = false end
  end
 end
 if n then return "nobody" end
 return nil;
end

turn = "O"
playing = true

mapx = {
["7"]=1,["8"]=2,["9"]=3,
["4"]=1,["5"]=2,["6"]=3,
["1"]=1,["2"]=2,["3"]=3}
mapy = {
["7"]=1,["8"]=1,["9"]=1,
["4"]=2,["5"]=2,["6"]=2,
["1"]=3,["2"]=3,["3"]=3}
mapp = {["X"]=1,["O"]=2,[1]="X",[2]="O",[0]=false}

while playing do
 print(string.char(0x1B,0x5B,0x33,0x4A,0x1B,0x5B,0x48,0x1B,0x5B,0x32,0x4A)) --clear screen
 draw()
 res = test()
 if res then break end
 print("879\n456\n123")
 if turn == "X" then turn="O" else turn = "X" end
 i="#";
 v=false;
 while not v do
  while not (mapx[i] and mapy[i]) do
   io.write(turn.."'s turn:")
   i = io.read()
  end
  x = mapx[i]
  y = mapy[i]
  if tpf[y][x] == 0 then v=true else print("something's in your way ;)") i="#" end
  print(i,x,y)
 end
 p = mapp[turn] 
 tpf[y][x]=p
end
print(res.." won!")